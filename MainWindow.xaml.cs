﻿using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;

namespace Vervloessem_Wout_oef29._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        int i = 0;  //changes with the radiobuttons
        ArrayList AWerknemers = new ArrayList();
        Object obj;
        public MainWindow()
        {

            InitializeComponent();

        }

        private void btnToevoegen_Click(object sender, RoutedEventArgs e)
        {
            switch (i)
            {
                case 0:
                    Commissiewerker cs = new Commissiewerker(tbNaam.Text, tbVoornaam.Text, Convert.ToUInt64(tbLoon.Text), Convert.ToUInt64(tbCommissie.Text), Convert.ToInt32(tbAantal.Text));
                    obj = cs;
                    break;

                case 1:
                    Stukwerker sw = new Stukwerker(tbNaam.Text, tbVoornaam.Text, Convert.ToUInt64(tbLoon.Text), Convert.ToInt32(tbAantal.Text));
                    obj = sw;
                    break;

                case 2:
                    Uurwerker uw = new Uurwerker(tbNaam.Text, tbVoornaam.Text, Convert.ToUInt64(tbLoon.Text), Convert.ToInt32(tbAantal.Text));
                    obj = uw;
                    break;
                case 3:
               //     Werknemer werknemer = new Werknemer(tbNaam.Text, tbVoornaam.Text, Convert.ToUInt64(tbLoon.Text));
               //     obj = werknemer;

                    break;
            }

            if (AWerknemers.Count.ToString() == "0")
            {
                AWerknemers.Add(obj);
            }
            else
            {
                foreach (var item in AWerknemers)
                {
                    if (item.GetType() == obj.GetType())
                    {
                        if (item.Equals(obj))
                        {
                            MessageBox.Show("Deze persoon ("+obj.GetType().Name.ToString()+") is al ingegeven");
                            break;
                        }
                        else
                        {
                            AWerknemers.Add(obj);
                            break;
                        }
                    }
                    else
                    {
                        AWerknemers.Add(obj);
                        break;
                    }
                }
            }
            CheckListDoubles(obj);

            //tbWerknemers.ClearValue;
            liWerknemers.Items.Clear();
            foreach (var item in AWerknemers)
            {
                liWerknemers.Items.Add(item);

               // tbWerknemers.Text += "---------------------------" + Environment.NewLine;
            }
            CleanTb();
          



        }

        private void rbCommisie_Checked(object sender, RoutedEventArgs e)
        {
            i = 0;
            tbCommissie.Opacity = 100;
            lblCommissie.Opacity = 100;
            lblAantal.Opacity = 100;
            tbAantal.Opacity = 100;
        }

        private void rbStuk_Checked(object sender, RoutedEventArgs e)
        {
            i = 1;
            tbCommissie.Opacity = 0;
            lblCommissie.Opacity = 0;
            lblAantal.Opacity = 100;
            tbAantal.Opacity = 100;
        }

        private void rbUurwerk_Checked(object sender, RoutedEventArgs e)
        {
            i = 2;
            tbCommissie.Opacity = 0;
            lblCommissie.Opacity = 0;
            lblAantal.Opacity = 100;
            tbAantal.Opacity = 100;
        }
        private void rbWerknemer_Checked(object sender, RoutedEventArgs e)
        {
            i = 3;
            tbCommissie.Opacity = 0;
            lblCommissie.Opacity = 0;
            lblAantal.Opacity = 0;
            tbAantal.Opacity = 0;
        }
        public void CleanTb()
        {
            tbNaam.Clear();
            tbVoornaam.Clear();
            tbAantal.Clear();
            tbCommissie.Clear();
            tbLoon.Clear();
            tbNaam.Focus();
        }
        private void CheckListDoubles(Object obj)
        {
           
            if (AWerknemers.Count.ToString() == "0")
            {
                AWerknemers.Add(obj);
            }
            else
            {
                foreach (var item in AWerknemers)
                {
                    if (item.GetType() == obj.GetType())
                    {
                        if (item.Equals(obj))
                        {
                       
                            break;
                        }
                        else
                        {
                            AWerknemers.Add(obj);
                            break;
                        }

                    }
                    else
                    {
                     
                        break;
                    }
                }
            }
        }
    }
}