﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vervloessem_Wout_oef29._1
{
    class Uurwerker : Werknemer
    {
        private double _uren;
        public double Uren { get; set; }
        public Uurwerker(string naam, string voornaam, decimal loon, int aantalUren):base(naam, voornaam, loon)
        {
            this.Uren = aantalUren;
        }
        public override string ToString()
        {
            return base.ToString() + "-Uurwerker"+Environment.NewLine;
        }
        public override decimal Verdiensten()
        {
            return (Loon * Convert.ToUInt64(Uren));
        }
        public override bool Equals(object wn)
        {
            return base.Equals(wn);
        }
    }
}
