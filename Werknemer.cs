﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Vervloessem_Wout_oef29._1
{
    abstract class Werknemer
    {
        private decimal _loon;
        private string _naam;
        private string _voornaam;

        public decimal Loon { get; set; }
        public string Naam { get; set; }
        public string Voornaam { get; set; }
        public BitmapImage Geslacht { get; set; }

        public Werknemer()
        {

        }
        public Werknemer(string naam, string voornaam, decimal loon)
        {
            Naam = naam;
            Voornaam = voornaam;
            Loon = loon;
        }
      
        public override string ToString()
        {
            return "Naam: " + Naam + Environment.NewLine +
                    "Voornaam: " + Voornaam + Environment.NewLine+
                    "Verdiensten (Loon): " + Verdiensten().ToString();
            

        
        }
        public override bool Equals(Object obj)
        {
            if (obj == null)
            {
                return false;
            }

            Werknemer p = obj as Werknemer;
            if ((System.Object)p == null)
            {
                return false;
            }
            if (obj is Uurwerker)
            {
              //  MessageBox.Show("De uurwerker is ooit al eens ingegeven");
                return ((this.Voornaam == p.Voornaam) && (this.Naam == p.Naam));// && (y == p.y);
            }
            if (obj is Commissiewerker)
            {
               // MessageBox.Show("De commissiewerker is ooit al eens ingegeven");
                return (this.Voornaam == p.Voornaam) && (this.Naam == p.Naam);// && (y == p.y);
            }
            if (obj is Stukwerker)
            {
              //  MessageBox.Show("De stukwerker is ooit al eens ingegeven");
                return (this.Voornaam == p.Voornaam) && (this.Naam == p.Naam);// && (y == p.y);
            }
            if (obj is Werknemer)
            {
            //    MessageBox.Show("Deze werknemer is ooit al eens ingegeven");
                return (this.Voornaam == p.Voornaam) && (this.Naam == p.Naam);// && (y == p.y);
            }
            else
            {
                return false;
            }
        }

        public virtual decimal Verdiensten()
        {
            return Loon;
        }

    }
}
