﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vervloessem_Wout_oef29._1
{
    class Stukwerker : Werknemer
    {

        private int _aantal;
        public int Aantal { get; set; }
        public Stukwerker(string naam, string voornaam, decimal loonPerStuk, int aantal) : base(naam, voornaam, loonPerStuk)
        {
            this.Aantal = aantal;
        }
        public override decimal Verdiensten()
        {
            return Loon * Aantal;
        }
        public override string ToString()
        {
            return base.ToString() + "-Stukwerker" + Environment.NewLine;
        }

        //public override bool Equals(Object obj)
        //{

        //    // If parameter is null return false.
        //    if (obj == null)
        //    {
        //        return false;
        //    }

        //    // If parameter cannot be cast to Point return false.
        //    Stukwerker p = obj as Stukwerker;
        //    if ((System.Object)p == null)
        //    {
        //        return false;
        //    }

        //    // Return true if the fields match:
        //    return (this.Voornaam == p.Voornaam) && (this.Naam == p.Voornaam);// && (y == p.y);
        //}

    }
}