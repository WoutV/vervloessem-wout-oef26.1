﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vervloessem_Wout_oef29._1
{
    class Commissiewerker :Werknemer
    {
        private int _aantal;
        private decimal _commissie;
        public int Aantal { get; set; }
        public decimal Commissie { get; set; }
        public Commissiewerker(string naam, string voornaam,decimal loon, decimal commissie, int aantal) : base(naam, voornaam, loon)
        {
            this.Aantal = aantal;
            this.Commissie = commissie;
        }
        public override decimal Verdiensten()
        {
            return Loon + (Aantal * Commissie);
        }
        public override string ToString()
        {
            return base.ToString() + "-Commissiewerker" + Environment.NewLine;
        }
        public override bool Equals(Object wn)
        {
            return base.Equals(wn);
        }
    }
}
